-- IZMENAaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
-- IZMENA IZMENA IZMENA IZMENA IZMENA KONFLIIIIIKT
-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: prodavnice_proizvod
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `drzava`
--

DROP TABLE IF EXISTS `drzava`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drzava` (
  `kod` varchar(3) NOT NULL,
  `naziv` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`kod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drzava`
--

LOCK TABLES `drzava` WRITE;
/*!40000 ALTER TABLE `drzava` DISABLE KEYS */;
INSERT INTO `drzava` VALUES ('bih','Bosna i Hercegovina'),('cro','Hrvatska'),('ger','Nemacka'),('hun','Madjarska'),('srb','Republika Srbija'),('usa','Sjedinjene Americke Drzave');
/*!40000 ALTER TABLE `drzava` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grad`
--

DROP TABLE IF EXISTS `grad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) DEFAULT NULL,
  `pttBroj` varchar(10) DEFAULT NULL,
  `drzavaid` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_grad_1_idx` (`drzavaid`),
  CONSTRAINT `fk_grad_1` FOREIGN KEY (`drzavaid`) REFERENCES `drzava` (`kod`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grad`
--

LOCK TABLES `grad` WRITE;
/*!40000 ALTER TABLE `grad` DISABLE KEYS */;
INSERT INTO `grad` VALUES (1,'Novi Sad','21000','srb'),(2,'Beograd ','11000','srb'),(3,'Zrenjanin','23000','srb'),(4,'Subotica','24000','srb'),(5,'Budisava','21242','srb');
/*!40000 ALTER TABLE `grad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategorija`
--

DROP TABLE IF EXISTS `kategorija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategorija` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategorija`
--

LOCK TABLES `kategorija` WRITE;
/*!40000 ALTER TABLE `kategorija` DISABLE KEYS */;
INSERT INTO `kategorija` VALUES (1,'Pekara'),(2,'Voce i Povrce'),(3,'Mlecni proizvodi'),(4,'Pice');
/*!40000 ALTER TABLE `kategorija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodavnica`
--

DROP TABLE IF EXISTS `prodavnica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodavnica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) DEFAULT NULL,
  `gradid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_prodavnica_1_idx` (`gradid`),
  CONSTRAINT `fk_prodavnica_1` FOREIGN KEY (`gradid`) REFERENCES `grad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodavnica`
--

LOCK TABLES `prodavnica` WRITE;
/*!40000 ALTER TABLE `prodavnica` DISABLE KEYS */;
INSERT INTO `prodavnica` VALUES (1,'Idea',2),(2,'Mikromarket',1),(3,'Maxi',5),(4,'SuperVero',2),(5,'Lidl',4),(6,'Shop & Go',4),(7,'Univerexport',5);
/*!40000 ALTER TABLE `prodavnica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodavnica_proizvod`
--

DROP TABLE IF EXISTS `prodavnica_proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodavnica_proizvod` (
  `proizvodid` int(11) DEFAULT NULL,
  `prodavnicaid` int(11) DEFAULT NULL,
  `cena` decimal(12,2) DEFAULT NULL,
  KEY `fk_prodavnica_proizvod_1_idx` (`proizvodid`),
  KEY `fk_prodavnica_proizvod_2_idx` (`prodavnicaid`),
  CONSTRAINT `fk_prodavnica_proizvod_1` FOREIGN KEY (`proizvodid`) REFERENCES `proizvod` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_prodavnica_proizvod_2` FOREIGN KEY (`prodavnicaid`) REFERENCES `prodavnica` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodavnica_proizvod`
--

LOCK TABLES `prodavnica_proizvod` WRITE;
/*!40000 ALTER TABLE `prodavnica_proizvod` DISABLE KEYS */;
INSERT INTO `prodavnica_proizvod` VALUES (1,3,120.50),(2,1,189.00),(5,2,218.20),(4,7,150.01),(1,6,99.99),(6,2,200.99),(3,7,50.55);
/*!40000 ALTER TABLE `prodavnica_proizvod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvod`
--

DROP TABLE IF EXISTS `proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proizvod` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) DEFAULT NULL,
  `barkod` varchar(13) DEFAULT NULL,
  `proizvodjacid` int(11) DEFAULT NULL,
  `kategorijaid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_proizvod_1_idx` (`proizvodjacid`),
  KEY `fk_proizvod_2_idx` (`kategorijaid`),
  CONSTRAINT `fk_proizvod_1` FOREIGN KEY (`proizvodjacid`) REFERENCES `proizvodjac` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proizvod_2` FOREIGN KEY (`kategorijaid`) REFERENCES `kategorija` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvod`
--

LOCK TABLES `proizvod` WRITE;
/*!40000 ALTER TABLE `proizvod` DISABLE KEYS */;
INSERT INTO `proizvod` VALUES (1,'Kiflice','1231231231452',3,1),(2,'Paradajz','1452145212345',1,2),(3,'Sok Jabuka 1L','1261261264521',6,4),(4,'Jogurt 1L','3213213211234',4,3),(5,'Sok Kruska 1L','4124124124123',1,4),(6,'Kupus','4124154127896',5,2);
/*!40000 ALTER TABLE `proizvod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvodjac`
--

DROP TABLE IF EXISTS `proizvodjac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proizvodjac` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(100) DEFAULT NULL,
  `gradid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_proizvodjac_1_idx` (`gradid`),
  CONSTRAINT `fk_proizvodjac_1` FOREIGN KEY (`gradid`) REFERENCES `grad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvodjac`
--

LOCK TABLES `proizvodjac` WRITE;
/*!40000 ALTER TABLE `proizvodjac` DISABLE KEYS */;
INSERT INTO `proizvodjac` VALUES (1,'Max Product d.o.o',1),(2,'Nasa proizvodnja d.o.o',3),(3,'Ljupko product d.o.o',1),(4,'Nase malo blago d.o.o',4),(5,'Najbolji proizvod d.o.o',2),(6,'Domace a nase d.o.o',5);
/*!40000 ALTER TABLE `proizvodjac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `zadatak1`
--

DROP TABLE IF EXISTS `zadatak1`;
/*!50001 DROP VIEW IF EXISTS `zadatak1`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `zadatak1` AS SELECT 
 1 AS `naziv_proizvoda`,
 1 AS `naziv_drzave`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `zadatak2`
--

DROP TABLE IF EXISTS `zadatak2`;
/*!50001 DROP VIEW IF EXISTS `zadatak2`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `zadatak2` AS SELECT 
 1 AS `count(proizvod.id)`,
 1 AS `naziv`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `zadatak3`
--

DROP TABLE IF EXISTS `zadatak3`;
/*!50001 DROP VIEW IF EXISTS `zadatak3`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `zadatak3` AS SELECT 
 1 AS `naziv_prodavnice`,
 1 AS `naziv_grada`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `zadatak4`
--

DROP TABLE IF EXISTS `zadatak4`;
/*!50001 DROP VIEW IF EXISTS `zadatak4`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `zadatak4` AS SELECT 
 1 AS `naziv_prodavnice`,
 1 AS `naziv_proizvoda`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `zadatak5`
--

DROP TABLE IF EXISTS `zadatak5`;
/*!50001 DROP VIEW IF EXISTS `zadatak5`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `zadatak5` AS SELECT 
 1 AS `naziv_proizvoda`,
 1 AS `naziv_kategorije`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `zadatak6`
--

DROP TABLE IF EXISTS `zadatak6`;
/*!50001 DROP VIEW IF EXISTS `zadatak6`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `zadatak6` AS SELECT 
 1 AS `count(proizvod.id)`,
 1 AS `naziv`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `zadatak1`
--

/*!50001 DROP VIEW IF EXISTS `zadatak1`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `zadatak1` AS select `p`.`naziv` AS `naziv_proizvoda`,`d`.`naziv` AS `naziv_drzave` from (((`proizvod` `p` join `proizvodjac` on((`p`.`proizvodjacid` = `proizvodjac`.`id`))) join `grad` on((`proizvodjac`.`gradid` = `grad`.`id`))) join `drzava` `d` on((`grad`.`drzavaid` = `d`.`kod`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `zadatak2`
--

/*!50001 DROP VIEW IF EXISTS `zadatak2`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `zadatak2` AS select count(`proizvod`.`id`) AS `count(proizvod.id)`,`proizvodjac`.`naziv` AS `naziv` from (`proizvod` join `proizvodjac` on((`proizvod`.`proizvodjacid` = `proizvodjac`.`id`))) group by `proizvodjac`.`naziv` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `zadatak3`
--

/*!50001 DROP VIEW IF EXISTS `zadatak3`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `zadatak3` AS select `prodavnica`.`naziv` AS `naziv_prodavnice`,`grad`.`naziv` AS `naziv_grada` from (`prodavnica` join `grad` on((`prodavnica`.`gradid` = `grad`.`id`))) where (`grad`.`naziv` = 'Budisava') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `zadatak4`
--

/*!50001 DROP VIEW IF EXISTS `zadatak4`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `zadatak4` AS select `prodavnica`.`naziv` AS `naziv_prodavnice`,`proizvod`.`naziv` AS `naziv_proizvoda` from ((`prodavnica_proizvod` join `prodavnica` on((`prodavnica_proizvod`.`prodavnicaid` = `prodavnica`.`id`))) join `proizvod` on((`prodavnica_proizvod`.`proizvodid` = `proizvod`.`id`))) where (`prodavnica`.`naziv` = 'Mikromarket') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `zadatak5`
--

/*!50001 DROP VIEW IF EXISTS `zadatak5`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `zadatak5` AS select `proizvod`.`naziv` AS `naziv_proizvoda`,`kategorija`.`naziv` AS `naziv_kategorije` from (`proizvod` join `kategorija` on((`proizvod`.`kategorijaid` = `kategorija`.`id`))) where (`kategorija`.`naziv` = 'Pice') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `zadatak6`
--

/*!50001 DROP VIEW IF EXISTS `zadatak6`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `zadatak6` AS select count(`proizvod`.`id`) AS `count(proizvod.id)`,`kategorija`.`naziv` AS `naziv` from (`proizvod` join `kategorija` on((`proizvod`.`kategorijaid` = `kategorija`.`id`))) group by `kategorija`.`naziv` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-01-21 16:45:49
