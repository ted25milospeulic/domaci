-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: prodavnice
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `drzava`
--

DROP TABLE IF EXISTS `drzava`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drzava` (
  `kod` varchar(3) NOT NULL,
  `naziv` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`kod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drzava`
--

LOCK TABLES `drzava` WRITE;
/*!40000 ALTER TABLE `drzava` DISABLE KEYS */;
INSERT INTO `drzava` VALUES ('bih','Bosna i Hercegovina'),('cro','Hrvatska'),('hun','Madjarska'),('srb','Republika Srbija'),('usa','Sjedinjene Americke Drzave');
/*!40000 ALTER TABLE `drzava` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grad`
--

DROP TABLE IF EXISTS `grad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grad` (
  `id` int(11) NOT NULL,
  `naziv` varchar(45) DEFAULT NULL,
  `pttBroj` varchar(10) DEFAULT NULL,
  `drzavaID` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_grad_1_idx` (`drzavaID`),
  CONSTRAINT `fk_grad_1` FOREIGN KEY (`drzavaID`) REFERENCES `drzava` (`kod`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grad`
--

LOCK TABLES `grad` WRITE;
/*!40000 ALTER TABLE `grad` DISABLE KEYS */;
INSERT INTO `grad` VALUES (1,'Novi Sad','21000',NULL),(2,'Beograd','11000',NULL),(3,'Subotica','24000',NULL),(4,'Zrenjanin','23000',NULL),(5,'Budisava','21242',NULL);
/*!40000 ALTER TABLE `grad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategorija`
--

DROP TABLE IF EXISTS `kategorija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategorija` (
  `kategorijaID` int(11) NOT NULL,
  `naziv` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`kategorijaID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategorija`
--

LOCK TABLES `kategorija` WRITE;
/*!40000 ALTER TABLE `kategorija` DISABLE KEYS */;
INSERT INTO `kategorija` VALUES (1,'Mesni proizvod'),(2,'Pica'),(3,'Hemija'),(4,'Voce i Povrce'),(5,'Mlecni proizvodi');
/*!40000 ALTER TABLE `kategorija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodavnica`
--

DROP TABLE IF EXISTS `prodavnica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodavnica` (
  `prodavnicaID` int(11) NOT NULL,
  `naziv` varchar(45) DEFAULT NULL,
  `gradID` int(11) DEFAULT NULL,
  PRIMARY KEY (`prodavnicaID`),
  KEY `fk_prodavnica_1_idx` (`gradID`),
  CONSTRAINT `fk_prodavnica_1` FOREIGN KEY (`gradID`) REFERENCES `grad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodavnica`
--

LOCK TABLES `prodavnica` WRITE;
/*!40000 ALTER TABLE `prodavnica` DISABLE KEYS */;
INSERT INTO `prodavnica` VALUES (1,'Mikromarket',NULL),(2,'Maxi',NULL),(3,'Idea',NULL),(4,'Shop&Go',NULL),(5,'STR Duda',NULL);
/*!40000 ALTER TABLE `prodavnica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodavnica_proizvod`
--

DROP TABLE IF EXISTS `prodavnica_proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodavnica_proizvod` (
  `prizvodID` int(11) DEFAULT NULL,
  `prodavnicaID` int(11) DEFAULT NULL,
  `cena` decimal(12,2) DEFAULT NULL,
  KEY `fk_prodavnica_proizvod_1_idx` (`prizvodID`),
  KEY `fk_prodavnica_proizvod_2_idx` (`prodavnicaID`),
  CONSTRAINT `fk_prodavnica_proizvod_1` FOREIGN KEY (`prizvodID`) REFERENCES `proizvod` (`proizvodID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_prodavnica_proizvod_2` FOREIGN KEY (`prodavnicaID`) REFERENCES `prodavnica` (`prodavnicaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodavnica_proizvod`
--

LOCK TABLES `prodavnica_proizvod` WRITE;
/*!40000 ALTER TABLE `prodavnica_proizvod` DISABLE KEYS */;
INSERT INTO `prodavnica_proizvod` VALUES (NULL,NULL,120.25),(NULL,NULL,180.90),(NULL,NULL,254.00),(NULL,NULL,152.03),(NULL,NULL,88.99);
/*!40000 ALTER TABLE `prodavnica_proizvod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvod`
--

DROP TABLE IF EXISTS `proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proizvod` (
  `proizvodID` int(11) NOT NULL,
  `naziv` varchar(45) DEFAULT NULL,
  `barkod` varchar(13) DEFAULT NULL,
  `proizvodjacID` int(11) DEFAULT NULL,
  `kategorijaID` int(11) DEFAULT NULL,
  PRIMARY KEY (`proizvodID`),
  KEY `fk_proizvod_1_idx` (`proizvodjacID`),
  KEY `fk_proizvod_2_idx` (`kategorijaID`),
  CONSTRAINT `fk_proizvod_1` FOREIGN KEY (`proizvodjacID`) REFERENCES `proizvodjac` (`proizvodjacID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proizvod_2` FOREIGN KEY (`kategorijaID`) REFERENCES `kategorija` (`kategorijaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvod`
--

LOCK TABLES `proizvod` WRITE;
/*!40000 ALTER TABLE `proizvod` DISABLE KEYS */;
INSERT INTO `proizvod` VALUES (1,'Sveza pastrmka','1231231231236',NULL,NULL),(2,'Hrono kifla','1451451231452',NULL,NULL),(3,'Prasak za ves','7896789612345',NULL,NULL),(4,'Jogurt','1251251251236',NULL,NULL),(5,'Paradajz 1kg','1471471471254',NULL,NULL);
/*!40000 ALTER TABLE `proizvod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvodjac`
--

DROP TABLE IF EXISTS `proizvodjac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proizvodjac` (
  `proizvodjacID` int(11) NOT NULL,
  `naziv` varchar(100) DEFAULT NULL,
  `gradID` int(11) DEFAULT NULL,
  PRIMARY KEY (`proizvodjacID`),
  KEY `fk_proizvodjac_1_idx` (`gradID`),
  CONSTRAINT `fk_proizvodjac_1` FOREIGN KEY (`gradID`) REFERENCES `grad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvodjac`
--

LOCK TABLES `proizvodjac` WRITE;
/*!40000 ALTER TABLE `proizvodjac` DISABLE KEYS */;
/*!40000 ALTER TABLE `proizvodjac` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-01-19 20:56:02
